// ball03.cc

#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
using namespace std;

const int SCREEN_WIDTH=640;
const int SCREEN_HEIGHT=480;
const int SCREEN_BPP=32;

const int TAILLE=20;

struct ball 
{
  int x;     // abscisse du centre de la balle
  int y;     // ordonn�e du centre de la balle
  int w;     // largeur de la balle
  int h;     // hauteur de la balle
  int mvt_x; // mouvement sur l'axe des abscisses
  int mvt_y; // mouvement sur l'axe des ordonn�es
};

void
initBall(ball &b)
{
  
  b.x=SCREEN_WIDTH/2;
  b.y=SCREEN_HEIGHT/2;
  b.w=TAILLE;
  b.h=TAILLE;
  b.mvt_x=2;
  b.mvt_y=2;
}

bool
collision(SDL_Rect a, SDL_Rect b)
{
  int leftA, leftB; 
  int rightA, rightB; 
  int topA, topB; 
  int bottomA, bottomB; 
  
  leftA = a.x; 
  rightA = a.x + a.w; 
  topA = a.y;
  bottomA = a.y + a.h; 
  
  leftB = b.x; 
  rightB = b.x + b.w; 
  topB = b.y; 
  bottomB = b.y + b.h; 
  
  if(bottomA <= topB) 
    return false; 
  if(topA >= bottomB) 
    return false;  
  if(rightA <= leftB) 
    return false;  
  if(leftA >= rightB) 
    return false;
  
  return true; 
}

void 
moveBall(ball &b, SDL_Rect wall)
{
  SDL_Rect tmp;

  b.x+=b.mvt_x;

  tmp.x=b.x-TAILLE/2;
  tmp.y=b.y-TAILLE/2;
  tmp.h=TAILLE;
  tmp.w=TAILLE;
  
  // Correction Mouvement Horizontal
  if((b.x+TAILLE/2>SCREEN_WIDTH) || (b.x-TAILLE/2<0) || collision(tmp,wall))
    {
      b.x-=b.mvt_x;
      b.mvt_x*=-1;
    }


  b.y+=b.mvt_y;

  tmp.x=b.x-TAILLE/2;
  tmp.y=b.y-TAILLE/2;

  // Correction Mouvement Vertical
  if((b.y+TAILLE/2>SCREEN_HEIGHT) || (b.y-TAILLE/2<0) || collision(tmp,wall))
    {
      b.y-=b.mvt_y;
      b.mvt_y*=-1;
    }
}

void
showBall(ball b, SDL_Surface *s)
{
  SDL_Rect r;
  r.x=b.x-TAILLE/2;
  r.y=b.y-TAILLE/2;
  r.w=TAILLE;
  r.h=TAILLE;
  
  SDL_FillRect(s,&r, 
	       SDL_MapRGB(s->format,255,0,0));
}

int 
main(int argc, char* argv[])
{
  bool quit=false;

  SDL_Surface *screen;
  SDL_Event event;
  
  SDL_Rect wall;
  wall.x=100;
  wall.y=100;
  wall.w=100;
  wall.h=300;
  
  ball b;
 

  SDL_Init(SDL_INIT_EVERYTHING);
  screen=SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,
			  SCREEN_BPP,SDL_SWSURFACE);


  initBall(b);
  while(!quit)
    {
      SDL_FillRect(screen,&screen->clip_rect, 
		   SDL_MapRGB(screen->format,255,255,255));
      
      SDL_FillRect(screen,&wall, 
		   SDL_MapRGB(screen->format,0,255,0));
      
      
      showBall(b,screen);

      SDL_Flip(screen);

      while(SDL_PollEvent(&event))
	if(event.type==SDL_QUIT)
	    quit=true;

      moveBall(b,wall);
      SDL_Delay(10);
    }

  SDL_FreeSurface(screen);
  SDL_Quit();
  return EXIT_SUCCESS;    
}
